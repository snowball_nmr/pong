package pong;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Pong implements ActionListener, KeyListener {

	public String ip;
	public static Pong pong;
//	public static boolean 
	public static int pl_id;
	public static ArrayList<String> ipList = new ArrayList<String>();
	public static int MaxPeople;
	public static String n;
	public static boolean isRejected = false,isWaitingForGameStart =true ,connectException =false;
	public static boolean gameStr = false;
	public int width = 700, height = 700;
	public static String connecExceptiontString;

	public Renderer renderer;

	public Paddle player1;

	public Paddle player3;

	public Paddle player2;

	public Paddle player4;

	public Ball ball;
	
	
	public boolean dia =true;

	public boolean bot1 = false, bot2 = false, bot3 = false, bot4 = false, selectingDifficulty;

	public boolean w, s, up, down, z, x, comma, period;

	public int gameStatus = 0, scoreLimit = 7, playerWon; // 0 = Menu, 1 =
															// Paused, 2 =
															// Playing, 3 = Over

	public boolean fourp_mode = true;
	public int page_num = 0; // 0 = select new/join, 1 = connect to server page,
								// 2 = select 2/4 player in create new game page
	public int botDifficulty, bot1Moves, bot2Moves, bot3Moves, bot4Moves,bot1Cooldown =0, bot2Cooldown = 0, bot3Cooldown = 0, bot4Cooldown = 0;

	public Random random;

	public JFrame jframe;
	// public TextField jipadd = new TextField(40);

	public Pong() {
		Timer timer = new Timer(20, this);
		random = new Random();

		jframe = new JFrame("Pong");
		// jipadd = new JTextField(40);

		renderer = new Renderer();

		jframe.setSize(width + 15, height + 35);
		jframe.setVisible(true);
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.add(renderer);
		jframe.addKeyListener(this);

		timer.start();
	}

	public void start() {
		
		player1 = new Paddle(this, 1);
		player1.score = this.scoreLimit;
		player3 = new Paddle(this, 3);
		player3.score = this.scoreLimit;
		player2 = new Paddle(this, 2);
		player2.score = this.scoreLimit;
		player4 = new Paddle(this, 4);
		player4.score = this.scoreLimit;
		ball = new Ball(this);
		gameStatus = 2;

	}

	public void update() {
		if (player1.score <= 0)
			player1.get_lost();
		if (player3.score <= 0)
			player3.get_lost();
		if (player2.score <= 0)
			player2.get_lost();
		if (player4.score <= 0)
			player4.get_lost();
		// if (player1.score >= scoreLimit)
		
		if(fourp_mode)
		{
			if (player1.score <= 0 && player2.score <= 0 && player3.score <= 0) {
				playerWon = 4;
				gameStatus = 3;
			}

			if (player1.score <= 0 && player3.score <= 0 && player4.score <= 0) {
				playerWon = 2;
				gameStatus = 3;
			}
		}

		// if (player2.score >= scoreLimit)
		if (player1.score <= 0 && player2.score <= 0 && player4.score <= 0) {
			gameStatus = 3;
			playerWon = 3;
		}

		if (player3.score <= 0 && player2.score <= 0 && player4.score <= 0) {
			playerWon = 1;
			gameStatus = 3;
		}
		if (!bot1) {
			if (w) {
				player1.move_up(true);
			}
			if (s) {
				player1.move_up(false);
			}
		} else {
			if (bot1Cooldown > 0) {
				bot1Cooldown--;

				if (bot1Cooldown == 0) {
					bot1Moves = 0;
				}
			}

			if (bot1Moves < 10) {
				if (player1.y + player1.height / 2 < ball.y) {
					player1.move_up(false);
					bot1Moves++;
				}

				if (player1.y + player1.height / 2 > ball.y) {
					player1.move_up(true);
					bot1Moves++;
				}

				if (botDifficulty == 0) {
					bot1Cooldown = 20;
				}
				if (botDifficulty == 1) {
					bot1Cooldown = 15;
				}
				if (botDifficulty == 2) {
					bot1Cooldown = 10;
				}
			}
		}

		
		if (!bot3) {
			if (up) {
				player3.move_up(true);
			}
			if (down) {
				player3.move_up(false);
			}
		} else {
			if (bot3Cooldown > 0) {
				bot3Cooldown--;

				if (bot3Cooldown == 0) {
					bot3Moves = 0;
				}
			}

			if (bot3Moves < 10) {
				if (player3.y + player3.height / 2 < ball.y) {
					player3.move_up(false);
					bot3Moves++;
				}

				if (player3.y + player3.height / 2 > ball.y) {
					player3.move_up(true);
					bot3Moves++;
				}

				if (botDifficulty == 0) {
					bot3Cooldown = 20;
				}
				if (botDifficulty == 1) {
					bot3Cooldown = 15;
				}
				if (botDifficulty == 2) {
					bot3Cooldown = 10;
				}
			}
		}
		if(fourp_mode)
		{
			if (!bot2) {
				if (z) {
					player2.move_left(true);
				}
				if (x) {
					player2.move_left(false);
				}
			} else {
				if (bot2Cooldown > 0) {
					bot2Cooldown--;

					if (bot2Cooldown == 0) {
						bot2Moves = 0;
					}
				}

				if (bot2Moves < 10) {
					if (player2.x + player2.height / 2 < ball.x) {
						player2.move_left(false);
						bot2Moves++;
					}

					if (player2.x + player2.height / 2 > ball.x) {
						player2.move_left(true);
						bot2Moves++;
					}

					if (botDifficulty == 0) {
						bot2Cooldown = 23;
					}
					if (botDifficulty == 1) {
						bot2Cooldown = 18;
					}
					if (botDifficulty == 2) {
						bot2Cooldown = 13;
					}
				}
			}

			if (!bot4) {
				if (comma) {
					player4.move_left(true);
				}
				if (period) {
					player4.move_left(false);
				}
			} else {
				if (bot4Cooldown > 0) {
					bot4Cooldown--;

					if (bot4Cooldown == 0) {
						bot4Moves = 0;
					}
				}

				if (bot4Moves < 10) {
					//System.out.println((player4==null)+"at line 265");
					if (player4.x + player4.height / 2 < ball.x) {
						player4.move_left(false);
						bot4Moves++;
					}

					if (player4.x + player4.height / 2 > ball.x) {
						player4.move_left(true);
						bot4Moves++;
					}

					if (botDifficulty == 0) {
						bot4Cooldown = 20;
					}
					if (botDifficulty == 1) {
						bot4Cooldown = 15;
					}
					if (botDifficulty == 2) {
						bot4Cooldown = 10;
					}
				}
			}
		}

		ball.update(player1, player3, player2, player4);
	}

	public void render(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (gameStatus == 0) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("Arial", 1, 50));

			g.drawString("PONG", width / 2 - 75, 50);

			if (page_num == 0) {
				g.setFont(new Font("Arial", 1, 30));
				g.drawString("Press Enter to create a new game", width / 2 - 150, height / 2 - 25);
				g.drawString("Press Space to join a game", width / 2 - 150, height / 2 + 25);
			} else if (page_num == 1) {
				g.setFont(new Font("Arial", 1, 30));
				if((!connectException)&&(!gameStr) && (!isRejected) && (isWaitingForGameStart))
					g.drawString("Wait till the server is reached..",width / 2 - 150,height / 2 - 75);
				else if(isRejected)
					g.drawString("Rejected by server", width / 2 - 150,height / 2 - 75);
				else if(connectException)
					g.drawString("Connect Exception.."+connecExceptiontString,width / 2 - 50,height / 2 - 75);
				else if (gameStr)
					g.drawString("Accepted by server", width / 2 - 150,height / 2 - 75);
				g.drawString("Ip address of the sever : " + ip, width / 2 - 150, height / 2 - 25);
				g.drawString("Press N to start...", width / 2 - 150, height / 2 + 25);

			} else if (page_num == 2) {
				g.setFont(new Font("Arial", 1, 30));
				g.drawString("you have created a server with "+n+" people", width / 2 - 150, 100);
				if(!gameStr)
				{
					g.drawString("Please wait..",width / 2 - 15,150);
				}
				else
				{
				g.drawString("No. of people = "+ipList.size()+"only",width / 2 - 15,150);
				}
				g.drawString("Press N to start...", width / 2 - 150, height / 2 + 125);
				g.drawString("Press Enter to Play in 4 player mode", width / 2 - 150, height / 2 - 25);
				g.drawString("Press T to Play in 2 player mode", width / 2 - 150, height / 2 + 25);
			} else if (!selectingDifficulty) {
				g.setFont(new Font("Arial", 1, 30));

				g.drawString("Press Space to Play", width / 2 - 150, height / 2 - 25);
				if (fourp_mode) {
					g.drawString("Press 2 to Play with Bot2 :" + bot2, width / 2 - 200, height / 2 + 25);
					g.drawString("Press 4 to Play with Bot4 :" + bot4, width / 2 - 200, height / 2 + 125);
				}
				g.drawString("Press 3 to Play with Bot3 :" + bot3, width / 2 - 200, height / 2 + 75);
				g.drawString("Press Enter to select difficulty", width / 2 - 200, height / 2 + 175);
				g.drawString("<< Score Limit: " + scoreLimit + " >>", width / 2 - 150, height / 2 + 275);
			}
		}

		if (selectingDifficulty) {
			String string = botDifficulty == 0 ? "Easy" : (botDifficulty == 1 ? "Medium" : "Hard");

			g.setFont(new Font("Arial", 1, 30));

			g.drawString("<< Bot Difficulty: " + string + " >>", width / 2 - 180, height / 2 - 25);
			g.drawString("Press Space to Play", width / 2 - 150, height / 2 + 25);
		}

		if (gameStatus == 1) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("Arial", 1, 50));
			g.drawString("PAUSED", width / 2 - 103, height / 2 - 25);
		}

		if (gameStatus == 1 || gameStatus == 2) {
			
			
			g.setColor(Color.WHITE);

			g.setStroke(new BasicStroke(5f));

			g.drawLine(20, 20, width-20, height-20);
			g.drawLine(20, height-20, width-20, 20);
			g.drawRect(20, 20, width - 40, height-40);
			g.setStroke(new BasicStroke(2f));

			g.drawOval(width / 2 - 150, height / 2 - 150, 300, 300);

			g.setFont(new Font("Arial", 1, 50));

			g.drawString(String.valueOf(player1.score), width / 2 - 305, height / 2);
			g.drawString(String.valueOf(player3.score), width / 2 + 280, height / 2);
			if (fourp_mode) {
			g.drawString(String.valueOf(player2.score), width / 2 - 12, 85);
			g.drawString(String.valueOf(player4.score), width / 2 - 12, 650);
			}
			player1.render(g);

			player3.render(g);
			if (fourp_mode) {
				player2.render(g);
				player4.render(g);
			}
			ball.render(g);
		}

		if (gameStatus == 3) {
			g.setColor(Color.WHITE);
			g.setFont(new Font("Arial", 1, 50));

			g.drawString("PONG", width / 2 - 75, 50);

			if ((bot3 && playerWon == 3) || (bot2 && playerWon == 2) || (bot4 && playerWon == 4)) {
				g.drawString("The Bot Wins!", width / 2 - 170, 200);
			} else {
				g.drawString("Player " + playerWon + " Wins!", width / 2 - 165, 200);
			}

			g.setFont(new Font("Arial", 1, 30));

			g.drawString("Press Space to Play Again", width / 2 - 185, height / 2 - 25);
			g.drawString("Press ESC for Menu", width / 2 - 140, height / 2 + 25);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (gameStatus == 2) {
			update();
		}

		renderer.repaint();
	}

	public static void main(String[] args) {
		pong = new Pong();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int id = e.getKeyCode();

		if (id == KeyEvent.VK_W) {
			w = true;
		} else if (id == KeyEvent.VK_S) {
			s = true;
		} else if (id == KeyEvent.VK_UP) {
			up = true;
		} else if (id == KeyEvent.VK_DOWN) {
			down = true;
		} else if (id == KeyEvent.VK_Z) {
			z = true;
		} else if (id == KeyEvent.VK_X) {
			x = true;
		} else if (id == KeyEvent.VK_COMMA) {
			comma = true;
		} else if (id == KeyEvent.VK_PERIOD) {
			period = true;
		} else if (id == KeyEvent.VK_RIGHT) {
			if (selectingDifficulty) {
				if (botDifficulty < 2) {
					botDifficulty++;
				} else {
					botDifficulty = 0;
				}
			} else if (gameStatus == 0) {
				scoreLimit++;
			}
		} else if (id == KeyEvent.VK_LEFT) {
			if (selectingDifficulty) {
				if (botDifficulty > 0) {
					botDifficulty--;
				} else {
					botDifficulty = 2;
				}
			} else if (gameStatus == 0 && scoreLimit > 1) {
				scoreLimit--;
			}
		} else if (id == KeyEvent.VK_T && gameStatus == 0) {
			if (page_num == 2) {
				fourp_mode = false;
				page_num = 4;
			} else
				selectingDifficulty = true;

		} else if(id == KeyEvent.VK_N && gameStatus == 0) {
			gameStatus = 2;
			
		} else if (id == KeyEvent.VK_ESCAPE && (gameStatus == 2 || gameStatus == 3)) {
			
			gameStatus = 0;
			page_num = 0;
		}  
		else if (id == KeyEvent.VK_3 && gameStatus == 0) {
			 if (bot3)
			 bot3 = false;
			 else
			 bot3 = true;
			
			 } else if (id == KeyEvent.VK_2 && gameStatus == 0) {
			 if (bot2)
			 bot2 = false;
			 else
			 bot2 = true;
			 //selectingDifficulty = true;
			 } else if (id == KeyEvent.VK_4 && gameStatus == 0) {
			 if (bot4)
			 bot4 = false;
			 else
			 bot4 = true;
			// selectingDifficulty = true
			 }
		else if (id == KeyEvent.VK_ENTER && gameStatus == 0) {
			if (page_num == 0) {
				n = JOptionPane.showInputDialog("Enter the num of players :");
				Interface.createServer(Integer.valueOf(n));
				page_num = 2;
				
			} else if (page_num == 2) {
				fourp_mode = true;
				page_num = 4;
			} else if (page_num == 1) {
			} else
				selectingDifficulty = true;
		} else if (id == KeyEvent.VK_SPACE) {
			if (dia && gameStatus == 0 && page_num == 0) {
				ip = JOptionPane.showInputDialog("Enter the IP address");
				Interface.joinServer(ip);
				dia = false;
				
			}
			if (gameStatus == 0 && page_num == 0) {
				page_num = 1;
			} else if (gameStatus == 0 || gameStatus == 3) {
				if (!selectingDifficulty) {
					bot3 = false;
					bot2 = false;
					bot4 = false;
				} else {
					selectingDifficulty = false;
				}

				start();
			} else if (gameStatus == 1) {
				gameStatus = 2;
			} else if (gameStatus == 2) {
				gameStatus = 1;
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int id = e.getKeyCode();

		if (id == KeyEvent.VK_W) {
			w = false;
		} else if (id == KeyEvent.VK_S) {
			s = false;
		} else if (id == KeyEvent.VK_UP) {
			up = false;
		} else if (id == KeyEvent.VK_DOWN) {
			down = false;
		} else if (id == KeyEvent.VK_Z) {
			z = false;
		} else if (id == KeyEvent.VK_X) {
			x = false;
		} else if (id == KeyEvent.VK_COMMA) {
			comma = false;
		} else if (id == KeyEvent.VK_PERIOD) {
			period = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}
}
