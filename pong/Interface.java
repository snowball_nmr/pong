package pong;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Inet4Address;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;



interface PongClientInterface
{
	public String getServerAddress();

	public String[] getAllConnectedData();

	public HashMap<String, String> getLastServerData();

	public boolean sendToServer(HashMap<String, String> map);

	public void closeSocket();

}

interface PongServerInterface
{
	public boolean startServer();

	public boolean stopServer();

	public String getServerAddress();

	public String[] getAllConnectedData();

	public boolean onReceiveDataFromClient(Socket socket);

	public boolean sendToClient(Socket socket, Object o);
}

class PongClient implements PongClientInterface
{
	int port = 9999;
	String host = "192.168.237.1";
	HashMap<String, String> lastServerData;
	Object InitialServerData;
	Socket socket = null;
	ObjectOutputStream oos = null;
	ObjectInputStream ois = null;

	public PongClient(String host, int port)
	{
		this.port = port;
		this.host = host;
	}

	@Override
	public String getServerAddress()
	{
		return host + ":" + port;
	}

	@Override
	public String[] getAllConnectedData()
	{
		return null;
	}

	@Override
	public HashMap<String, String> getLastServerData()
	{
		return lastServerData;
	}

	@Override
	public boolean sendToServer(HashMap<String, String> oo)
	{
		oos = null;
		ois = null;
		try
		{
			//			System.out.println("sending to " + host + ":" + port);
			socket = new Socket(host, port);
			// sending data
			oos = new ObjectOutputStream(socket.getOutputStream());
			//			// send my data
			Paddle mp = null;
			switch (PongNetworkStarter.id)
			{
				case 0:
					mp = Pong.pong.player1;
					break;
				case 1:
					mp = Pong.pong.player2;
					break;
				case 2:
					mp = Pong.pong.player3;
					break;
				case 3:
					mp = Pong.pong.player4;
					break;
			}

			if (PongNetworkStarter.id == 0)
			{
				String s = "";
				if (Pong.pong.bot4)
				{
					s += "," + Pong.pong.player4.score;
				}
				if (Pong.pong.bot3)
				{
					s += "," + Pong.pong.player3.score;
				}
				if (Pong.pong.bot2)
				{
					s += "," + Pong.pong.player2.score;
				}
				oos.writeObject(mp.paddleNumber + "," + mp.x + "," + mp.y + "," + mp.score + "," + Pong.pong.ball.x + "," + Pong.pong.ball.y + "," + Pong.pong.ball.motionX + "," + Pong.pong.ball.motionY
						+ s);
			}
			else
			{
				oos.writeObject(mp.paddleNumber + "," + mp.x + "," + mp.y + "," + mp.score);
			}
			// receiving data
			ois = new ObjectInputStream(socket.getInputStream());
			//			System.out.println("after input stream");
			Object readObject = ois.readObject();
			String[] line = ((String) readObject).split(",");
			Paddle p = null;
			switch (Integer.valueOf(line[0]))
			{
				case 1:
					p = Pong.pong.player1;
					Pong.pong.ball.x = Integer.valueOf(line[4]);
					Pong.pong.ball.y = Integer.valueOf(line[5]);
					Pong.pong.ball.motionX = Integer.valueOf(line[6]);
					Pong.pong.ball.motionY = Integer.valueOf(line[7]);
					if (Pong.pong.bot4)
					{
						Pong.pong.player4.score = Integer.valueOf(line[8]);
					}
					if (Pong.pong.bot3)
					{
						Pong.pong.player3.score = Integer.valueOf(line[9]);
					}
					if (Pong.pong.bot2)
					{
						Pong.pong.player2.score = Integer.valueOf(line[10]);
					}
					break;
				case 2:
					p = Pong.pong.player2;
					break;
				case 3:
					p = Pong.pong.player3;
					break;
				case 4:
					p = Pong.pong.player4;
					break;
			}
			p.x = Integer.valueOf(line[1]);
			p.y = Integer.valueOf(line[2]);
			p.score = Integer.valueOf(line[3]);

			// closing
			ois.close();
			oos.close();
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void closeSocket()
	{
		try
		{
			socket.close();
			ois.close();
			oos.close();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public boolean isGameRunning = true;

	public void startClientMonitor()
	{
		(new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				while (isGameRunning)
				{
					try
					{
						//						System.out.println("Sending to server");
						sendToServer(null);
						Thread.sleep(0);
					} catch (Exception e)
					{

					}
				}
			}
		})).start();
	}
}

class Keys
{
	public static final String KEY_CONNECTION_TYPE = "KEY_CONNECTION_TYPE", KEY_ID = "KEY_ID", KEY_CONNECTION_ADDRESS = "KEY_CONNECTION_ADDRESS",
			KEY_CONNECTION_REQUEST_RESPONSE = "KEY_CONNECTION_REQUEST_RESPONSE", KEY_KEYPRESSED = "KEY_KEYPRESSED", KEY_ISUPPRESSED = "KEY_ISUPPRESSED", KEY_START_GAME = "KEY_START_GAME",
			KEY_ADD0 = "KEY_ADD0", KEY_ADD1 = "KEY_ADD1", KEY_ADD2 = "KEY_ADD2", KEY_ADD3 = "KEY_ADD3", KEY_CLIENTADD = "KEY_CLIENTADD", KEY_X = "KEY_X", KEY_Y = "KEY_Y", KEY_PDNUM = "PDNUM",
			KEY_SCORE = "KEY_SCORE";

	public static final String ANS_CONNECTION_TYPE_FIRST = "ANS_CONNECTION_TYPE_FIRST", ANS_CONNECTION_TYPE_CONNECTED = "ANS_CONNECTION_TYPE_CONNECTED",
			ANS_CONNECTION_REQUEST_RESPONSE_REJECTED_FULL = "ANS_CONNECTION_REQUEST_RESPONSE_REJECTED_FULL", ANS_CONNECTION_REQUEST_RESPONSE_CONNECTED = "ANS_CONNECTION_REQUEST_RESPONSE_CONNECTED",
			ANS_YES = "ANS_YES", ANS_NO = "ANS_NO";
}

class PongServer implements PongServerInterface
{
	protected ServerSocket serverSocket;
	int port = 8888;
	volatile boolean isServerRunning = false;
	boolean gotAcceptTimeOut = false;

	private static String[] connectedClientsInOrder = new String[] { "", "", "" };

	PongServer(int port)
	{
		this.port = port;
	}

	@Override
	public boolean startServer()
	{
		try
		{
			Inet4Address ipv4 = (Inet4Address) Inet4Address.getLocalHost();
			serverSocket = new ServerSocket(port, 0, ipv4);
			serverSocket.setSoTimeout(120000);
			isServerRunning = true;

			System.out.println("D : PS - Server started - " + serverSocket.getInetAddress() + ":" + serverSocket.getLocalPort());
			(new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					while (isServerRunning)
					{
						try
						{
							Socket socket = serverSocket.accept();
							//							System.out.println("D : PS - Connection Recieved");
							onReceiveDataFromClient(socket);
						} catch (Exception e)
						{
							gotAcceptTimeOut = true;
							e.printStackTrace();
							isServerRunning = false;
							try
							{
								serverSocket.close();
							} catch (Exception ee)
							{

								ee.printStackTrace();
							}
						}
					}
					System.out.println("D : PS - Server stopped");
				}
			})).start();
			System.out.println("D : PS - Thread Started");
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}

	public boolean stopServer()
	{
		isServerRunning = false;
		return true;
	}

	@Override
	public String getServerAddress()
	{
		return serverSocket.getInetAddress() + ":" + serverSocket.getLocalPort();
	}

	@Override
	public String[] getAllConnectedData()
	{
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onReceiveDataFromClient(Socket socket)
	{
		try
		{
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			Object o = ois.readObject();
			String[] line = ((String) o).split(",");

			Paddle hp = null;
			/// read his/her data
			switch (Integer.valueOf(line[0]))
			{
				case 1:
					hp = Pong.pong.player1;
					Pong.pong.ball.x = Integer.valueOf(line[4]);
					Pong.pong.ball.y = Integer.valueOf(line[5]);
					Pong.pong.ball.motionX = Integer.valueOf(line[6]);
					Pong.pong.ball.motionY = Integer.valueOf(line[7]);
					if (Pong.pong.bot4)
					{
						Pong.pong.player4.score = Integer.valueOf(line[8]);
					}
					if (Pong.pong.bot3)
					{
						Pong.pong.player3.score = Integer.valueOf(line[9]);
					}
					if (Pong.pong.bot2)
					{
						Pong.pong.player2.score = Integer.valueOf(line[10]);
					}
					break;
				case 2:
					hp = Pong.pong.player2;
					break;
				case 3:
					hp = Pong.pong.player3;
					break;
				case 4:
					hp = Pong.pong.player4;
					break;
			}
			hp.x = Integer.valueOf(line[1]);
			hp.y = Integer.valueOf(line[2]);
			hp.score = Integer.valueOf(line[3]);

			Paddle mp = null;
			switch (PongNetworkStarter.id)
			{
				case 0:
					mp = Pong.pong.player1;
					break;
				case 1:
					mp = Pong.pong.player2;
					break;
				case 2:
					mp = Pong.pong.player3;
					break;
				case 3:
					mp = Pong.pong.player4;
					break;
			}

			if (PongNetworkStarter.id == 0)
			{
				String s = "";
				if (Pong.pong.bot4)
				{
					s += "," + Pong.pong.player4.score;
				}
				if (Pong.pong.bot3)
				{
					s += "," + Pong.pong.player3.score;
				}
				if (Pong.pong.bot2)
				{
					s += "," + Pong.pong.player2.score;
				}
				sendToClient(socket, mp.paddleNumber + "," + mp.x + "," + mp.y + "," + mp.score + "," + Pong.pong.ball.x + "," + Pong.pong.ball.y + "," + Pong.pong.ball.motionX + ","
						+ Pong.pong.ball.motionY + s);
			}
			else
			{
				sendToClient(socket, mp.paddleNumber + "," + mp.x + "," + mp.y + "," + mp.score);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean sendToClient(Socket socket, Object o)
	{
		try
		{
			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			oos.writeObject(o);
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}
}

class PongNetworkManager
{

	ArrayList<PongClient> pcs = new ArrayList<PongClient>();
	PongServer ps;
	final static int port = 3423;

	public PongNetworkManager()
	{
		System.out.println("size - " + PongNetworkStarter.listOfConnected.size() + " ,id - " + PongNetworkStarter.id);
		for (int i = 0; i < PongNetworkStarter.listOfConnected.size(); i++)
		{
			if (i != PongNetworkStarter.id)
			{
				pcs.add(new PongClient(PongNetworkStarter.listOfConnected.get(i), port));
			}
			else
			{
				ps = new PongServer(port);
				ps.startServer();
			}
		}
		for (PongClient p : pcs)
		{
			p.startClientMonitor();
		}
	}
}

class PongNetworkStarter
{
	static boolean gameStarted = false;
	final static int portStart = 6071;
	static int MAX_PEOPLE = 0;
	static int id = -1;
	static ArrayList<String> listOfConnected = new ArrayList<String>();

	class PongNetworkStarterServer extends PongServer
	{
		String[] connectedInOrder;
		long startTimeMillis = 0;
		boolean isTimeOver = false;

		PongNetworkStarterServer(int port, int MAX_PEOPLE)
		{
			super(port);
			super.startServer();
			startTimeMillis = System.currentTimeMillis();
			(new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						Thread.sleep(15000);
						isTimeOver = true;
					} catch (Exception e)
					{
					}
				}
			})).start();;
			connectedInOrder = new String[MAX_PEOPLE];
			connectedInOrder[0] = serverSocket.getInetAddress().toString();
			id = 0;
			PongNetworkStarter.MAX_PEOPLE = MAX_PEOPLE;
		}

		@Override
		public boolean onReceiveDataFromClient(Socket socket)
		{
			try
			{
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				Object o = ois.readObject();
				if (!(o instanceof HashMap<?, ?>))
				{
					return false;
				}
				HashMap<String, String> map = (HashMap<String, String>) o;
				if (map.get(Keys.KEY_CONNECTION_TYPE).equals(Keys.ANS_CONNECTION_TYPE_FIRST))
				{
					int freeID = 1;
					for (freeID = 1; freeID < MAX_PEOPLE; freeID++)
					{
						System.out.println("freeID - " + freeID + ", " + connectedInOrder[freeID]);
						if (connectedInOrder[freeID] == null || connectedInOrder[freeID].equals(""))
						{
							System.out.println("empty so filled - " + Keys.KEY_CLIENTADD);
							connectedInOrder[freeID] = map.get(Keys.KEY_CLIENTADD);
							break;
						}
						else if (connectedInOrder[freeID].equals(map.get(Keys.KEY_CLIENTADD)))
						{
							HashMap<String, String> responseMap = new HashMap<String, String>();
							sendToClient(socket, responseMap);
							System.out.println("connectedInOrder[freeID].equals(socket.getRemoteSocketAddress().toString()) - " + socket.getRemoteSocketAddress().toString());
							return true;
						}
					}
					if (freeID == MAX_PEOPLE)
					{
						HashMap<String, String> responseMap = new HashMap<String, String>();
						responseMap.put(Keys.KEY_CONNECTION_REQUEST_RESPONSE, Keys.ANS_CONNECTION_REQUEST_RESPONSE_REJECTED_FULL);
						sendToClient(socket, responseMap);
						System.out.println("D: PS - List Full");
						return true;
					}
					else
					{
						HashMap<String, String> responseMap = new HashMap<String, String>();
						responseMap.put(Keys.KEY_CONNECTION_REQUEST_RESPONSE, Keys.ANS_CONNECTION_REQUEST_RESPONSE_CONNECTED);
						responseMap.put(Keys.KEY_ID, "" + freeID);
						sendToClient(socket, responseMap);
						System.out.println("D: PS - Connecting Client at num " + freeID + " - " + connectedInOrder[freeID]);
						return true;
					}
				}
				if (map.get(Keys.KEY_CONNECTION_TYPE).equals(Keys.ANS_CONNECTION_TYPE_CONNECTED))
				{
					System.out.println("connected client");
					if (isTimeOver || connectedInOrder[MAX_PEOPLE - 1] != null)
					{
						int freeID = 0;
						for (freeID = 0; freeID < MAX_PEOPLE; freeID++)
						{
							if (connectedInOrder[freeID] == null || connectedInOrder[freeID].equals(""))
							{
								break;
							}
						}
						System.out.println("freeID - " + freeID);
						MAX_PEOPLE = freeID;
						System.out.println("start game");
						//all connected now whoever sends this request return it the complete connected array 
						//so that it can start it's own PongNetworkManager

						HashMap<String, String> responseMap = new HashMap<String, String>();

						if (MAX_PEOPLE > 0)
						{
							responseMap.put(Keys.KEY_ADD0, connectedInOrder[0].substring(1 + connectedInOrder[0].indexOf("/")));
							listOfConnected.add(responseMap.get(Keys.KEY_ADD0));
						}
						if (MAX_PEOPLE > 1)
						{
							responseMap.put(Keys.KEY_ADD1, connectedInOrder[1].substring(1 + connectedInOrder[1].indexOf("/")));
							listOfConnected.add(responseMap.get(Keys.KEY_ADD1));
						}
						if (MAX_PEOPLE > 2)
						{
							responseMap.put(Keys.KEY_ADD2, connectedInOrder[2].substring(1 + connectedInOrder[2].indexOf("/")));
							listOfConnected.add(responseMap.get(Keys.KEY_ADD2));
						}
						if (MAX_PEOPLE > 3)
						{
							responseMap.put(Keys.KEY_ADD3, connectedInOrder[3].substring(1 + connectedInOrder[3].indexOf("/")));
							listOfConnected.add(responseMap.get(Keys.KEY_ADD3));
						}
						System.out.println("All connected");
						responseMap.put(Keys.KEY_START_GAME, Keys.ANS_YES);
						sendToClient(socket, responseMap);
						gameStarted = true;
						Pong.gameStr = true;
						Pong.ipList = listOfConnected;
						switch (MAX_PEOPLE)
						{
						//case 0 : Pong.pong.bot2 = true; Pong.pong.bot3 = true; Pong.pong.bot4 =true; break;
							case 1:
								Pong.pong.bot2 = true;
								Pong.pong.bot3 = true;
								Pong.pong.bot4 = true;
								break;
							case 2:
								Pong.pong.bot2 = false;
								Pong.pong.bot3 = true;
								Pong.pong.bot4 = true;
								break;
							case 3:
								Pong.pong.bot2 = false;
								Pong.pong.bot3 = false;
								Pong.pong.bot4 = true;
								break;
							case 4:
								Pong.pong.bot2 = false;
								Pong.pong.bot3 = false;
								Pong.pong.bot4 = false;
								break;
						}
						Pong.pl_id = PongNetworkStarter.id;
						Pong.pong.start();
						new PongNetworkManager();
						return true;
					}
					else
					{
						Pong.gameStr = false;
						System.out.println("don't start game");
						HashMap<String, String> responseMap = new HashMap<String, String>();
						responseMap.put(Keys.KEY_START_GAME, Keys.ANS_NO);
						sendToClient(socket, responseMap);
						return true;
					}
				}
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			HashMap<String, String> responseMap = new HashMap<String, String>();
			sendToClient(socket, responseMap);
			System.out.println("end stub response");
			return true;
		}
	}

	class PongNetworkStarterClient extends PongClient
	{
		boolean isConnected = false, isWaitingForGameStart = true, isRejected = false, connectException = false;
		String address0, address1, address2, address3, connecExceptiontString;

		public PongNetworkStarterClient(String host, int port)
		{
			super(host, port);
		}

		@Override
		public boolean sendToServer(HashMap<String, String> o)
		{
			oos = null;
			ois = null;
			try
			{
				socket = new Socket(host, port);
				oos = new ObjectOutputStream(socket.getOutputStream());

				//sending object
				HashMap<String, String> map = new HashMap<String, String>();
				if (!isConnected)
				{
					map.put(Keys.KEY_CONNECTION_TYPE, Keys.ANS_CONNECTION_TYPE_FIRST);
					Inet4Address ipv4 = (Inet4Address) Inet4Address.getLocalHost();
					ServerSocket serverSocket = new ServerSocket(7412, 0, ipv4);
					map.put(Keys.KEY_CLIENTADD, "" + serverSocket.getInetAddress());
					serverSocket.close();
				}
				else
				{
					if (isWaitingForGameStart)
					{
						map.put(Keys.KEY_CONNECTION_TYPE, Keys.ANS_CONNECTION_TYPE_CONNECTED);
					}
					else
					{

					}
				}
				oos.writeObject(map);

				//getting object
				ois = new ObjectInputStream(socket.getInputStream());
				Object readObject = ois.readObject();
				if (readObject instanceof HashMap<?, ?>)
				{
					HashMap<String, String> readmap = (HashMap<String, String>) readObject;
					if (!isConnected)
					{
						if (readmap.get(Keys.KEY_CONNECTION_REQUEST_RESPONSE).equals(Keys.ANS_CONNECTION_REQUEST_RESPONSE_CONNECTED))
						{
							id = Integer.valueOf(readmap.get(Keys.KEY_ID));
							isConnected = true;
						}
						else if (readmap.get(Keys.KEY_CONNECTION_REQUEST_RESPONSE).equals(Keys.ANS_CONNECTION_REQUEST_RESPONSE_REJECTED_FULL))
						{
							isRejected = true;
						}
					}
					else
					{
						//connected
						if (isWaitingForGameStart)
						{
							if (readmap.get(Keys.KEY_START_GAME).equals(Keys.ANS_YES))
							{
								isWaitingForGameStart = false;
								address0 = readmap.get(Keys.KEY_ADD0);
								address1 = readmap.get(Keys.KEY_ADD1);
								address2 = readmap.get(Keys.KEY_ADD2);
								address3 = readmap.get(Keys.KEY_ADD3);
								if (address0 != null)
								{
									//									address0 = address0.substring(1, address0.indexOf(":"));
									System.out.println(address0);
									listOfConnected.add(address0);
								}
								if (address1 != null)
								{
									//									address1 = address1.substring(1, address1.indexOf(":"));
									System.out.println(address1);
									listOfConnected.add(address1);
								}
								if (address2 != null)
								{
									//									address2 = address2.substring(1, address2.indexOf(":"));
									System.out.println(address2);
									listOfConnected.add(address2);
								}
								if (address3 != null)
								{
									//									address3 = address3.substring(1, address3.indexOf(":"));
									System.out.println(address3);
									listOfConnected.add(address3);
								}
								MAX_PEOPLE = listOfConnected.size();
							}
						}
						else
						{
							//connected and 
						}
					}
				}

				ois.close();
				oos.close();
				return true;
			} catch (Exception e)
			{
				connectException = true;
				connecExceptiontString = e.getMessage();
				e.printStackTrace();
				return false;
			}
		}

		public void startClientMonitor()
		{
			(new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						while (!isRejected && isWaitingForGameStart && !connectException)
						{
							sendToServer(null);
							Thread.sleep(10);
						}
						if (isRejected)
						{
							Pong.isRejected = true;
						}
						if (connectException)
						{
							Pong.connectException = true;
							Pong.connecExceptiontString = connecExceptiontString;
						}
						if (!isWaitingForGameStart)
						{
							Pong.gameStr = true;
							Pong.pl_id = PongNetworkStarter.id;
							Pong.pong.start();
							new PongNetworkManager();

							switch (MAX_PEOPLE)
							{
								case 1:
									Pong.pong.bot2 = true;
									Pong.pong.bot3 = true;
									Pong.pong.bot4 = true;
									break;
								case 2:
									Pong.pong.bot2 = false;
									Pong.pong.bot3 = true;
									Pong.pong.bot4 = true;
									break;
								case 3:
									Pong.pong.bot2 = false;
									Pong.pong.bot3 = false;
									Pong.pong.bot4 = true;
									break;
								case 4:
									Pong.pong.bot2 = false;
									Pong.pong.bot3 = false;
									Pong.pong.bot4 = false;
									break;
							}

						}
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			})).start();
		}
	}

	public PongNetworkStarter(boolean isServer, int MaxPeople, String host)
	{
		if (isServer)
		{
			PongNetworkStarterServer pnss = new PongNetworkStarterServer(portStart, MaxPeople);
			pnss.startServer();
		}
		else
		{
			PongNetworkStarterClient pnsc = new PongNetworkStarterClient(host, portStart);
			pnsc.startClientMonitor();
		}
	}
}

public class Interface
{
	public static ArrayList<String> createServer(int n)
	{
		PongNetworkStarter pp = new PongNetworkStarter(true, n, null);
		ArrayList<String> al = PongNetworkStarter.listOfConnected;
		return al;
	}

	public static boolean joinServer(String host_ip)
	{
		PongNetworkStarter pp = new PongNetworkStarter(false, -1, host_ip);
		return (PongNetworkStarter.gameStarted);
	}
	//	public static void main(String args[])
	//	{
	//		new PongNetworkStarter(true, 4, null);
	//		//		new PongNetworkStarter(false, -1, "10.192.58.113");
	//	}
}


