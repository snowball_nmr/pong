package pong;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Ball
{

	public int x, y, width = 31, height = 31;

	public int motionX, motionY;

	public Random random;

	private Pong pong;

	public int amountOfHits;

	public Ball(Pong pong)
	{
		this.pong = pong;

		this.random = new Random();

		spawn();
	}

	public void update(Paddle paddle1, Paddle paddle3, Paddle paddle2, Paddle paddle4)
	{
		int speed = 5;

		this.x += motionX * speed;
		this.y += motionY * speed;

		if (this.y + height - motionY > pong.height || this.y + motionY < 0)
		{
			if (this.motionY < 0)
			{
				this.y = 0;
				this.motionY = random.nextInt(4);

				if (motionY == 0)
				{
					motionY = 1;
				}
				if(pong.fourp_mode)
				{
				if (paddle2.score > 0)
					paddle2.score--;
				if (paddle2.score != 0)
					this.amountOfHits = 0;
				}
				else
				this.amountOfHits = 0;
				
			}
			else
			{
				this.motionY = -random.nextInt(4);
				this.y = pong.height - height;

				if (motionY == 0)
				{
					motionY = -1;
				}
				if(pong.fourp_mode)
				{
				if (paddle4.score > 0)
					paddle4.score--;
				if (paddle4.score != 0)
				
                        this.amountOfHits = 0;
				}
				else
				this.amountOfHits = 0;
			}
                        
		}
		
		if (this.x + width - motionX > pong.width || this.x + motionX < 0)
		{
			if (this.motionX < 0)
			{
				this.x = 0;
				this.motionX = random.nextInt(4);

				if (motionX == 0)
				{
					motionX = 1;
				}
				if(paddle1.score >0)
				paddle1.score--;
                                if(paddle1.score !=0)
			this.amountOfHits = 0;
			}
			else
			{
				this.motionX = -random.nextInt(4);
				this.x = pong.width - width;

				if (motionX == 0)
				{
					motionX = -1;
				}
				if(paddle3.score >0)
				paddle3.score--;
                                if(paddle3.score !=0)
			this.amountOfHits = 0;
			}
                        
		}

		if (checkCollision(paddle1) == 1)
		{
			
			this.motionX = 1 + (amountOfHits / 5);
			this.motionY = -2 + random.nextInt(4);

			if (motionY == 0)
			{
				motionY = 1;
			}
			if(amountOfHits < 30)
			amountOfHits++;
		}
		else if (checkCollision(paddle3) == 1)
		{
			this.motionX = -1 - (amountOfHits / 5);
			this.motionY = -2 + random.nextInt(4);

			if (motionY == 0)
			{
				motionY = 1;
			}
			if(amountOfHits < 30)
			amountOfHits++;
		}
		if(pong.fourp_mode)
		{
			if (checkCollision(paddle2) == 1) {

				this.motionY = 1 + (amountOfHits / 5);
				this.motionX = -2 + random.nextInt(4);

				if (motionX == 0) {
					motionX = 1;
				}
				if (amountOfHits < 30)
					amountOfHits++;
			} else if (checkCollision(paddle4) == 1) {
				this.motionY = -1 - (amountOfHits / 5);
				this.motionX = -2 + random.nextInt(4);

				if (motionX == 0) {
					motionX = 1;
				}
				if (amountOfHits < 30)
					amountOfHits++;
			}
		}

//		if (checkCollision(paddle1) == 2)
//		{
//			paddle1.score--;
//			this.amountOfHits = 0;
//			//spawn();
//		}
//		else if (checkCollision(paddle3) == 2)
//		{
//			paddle3.score--;
//			this.amountOfHits = 0;
//			//spawn();
//		}
//		else if (checkCollision(paddle2) == 2)
//		{
//			paddle2.score--;
//			this.amountOfHits = 0;
//			//spawn();
//		}
//		else if (checkCollision(paddle4) == 2)
//		{
//			paddle3.score--;
//			this.amountOfHits = 0;
//			//spawn();
//		}
	}

	public void spawn()
	{
		this.amountOfHits = 0;
		this.x = pong.width / 2 - this.width / 2;
		this.y = pong.height / 2 - this.height / 2;

		this.motionY = -2 + random.nextInt(4);

		if (motionY == 0)
		{
			motionY = 1;
		}

		if (random.nextBoolean())
		{
			motionX = 1;
		}
		else
		{
			motionX = -1;
		}
	}

	public int checkCollision(Paddle paddle)//, boolean is_odd)
	{
//		if(is_odd)
//		{
		if(paddle.paddleNumber==1 || paddle.paddleNumber==3)
		{
			if (this.x < paddle.x + paddle.width && this.x + width > paddle.x && this.y < paddle.y + paddle.height
					&& this.y + height > paddle.y) {
				return 1; // bounce
			} else if ((paddle.x > x && paddle.paddleNumber == 1)
					|| (paddle.x < x - width && paddle.paddleNumber == 3)) {
				return 2; // score
			}
		}
		else
		{
			if (this.y < paddle.y + paddle.width && this.y + width > paddle.y && this.x < paddle.x + paddle.height
					&& this.x + height > paddle.x) {
				return 1; // bounce
			}
		}
//		}
//		else
//		{
//			
//		}

		return 0; //nothing
	}

	public void render(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillOval(x, y, width, height);
	}

}