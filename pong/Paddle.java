package pong;

import java.awt.Color;
import java.awt.Graphics;

public class Paddle
{

	public int paddleNumber;

	public int x, y, width = 20, height = 250;

	public int score;

	public Paddle(Pong pong, int paddleNumber)
	{
		this.paddleNumber = paddleNumber;

		if (paddleNumber == 1)
		{
			this.x = 0;
			this.y = pong.height / 2 - this.height / 2;
		}

		if (paddleNumber == 3)
		{
			this.x = pong.width - width;
			this.y = pong.height / 2 - this.height / 2;
		}
		
		if (paddleNumber == 2)
		{
			this.y =0;
			this.x = pong.width / 2 - this.height / 2;
		}
		
		if (paddleNumber == 4)
		{
			this.y = pong.height - width;
			this.x = pong.width / 2 - this.height / 2;
		}

		//this.y = pong.height / 2 - this.height / 2;
	}

	public void render(Graphics g)
	{
		if(Pong.pl_id+1 == this.paddleNumber)
		g.setColor(Color.YELLOW);
		else
			g.setColor(Color.WHITE);
		if (this.paddleNumber == 1 || this.paddleNumber == 3)
		g.fillRect(x, y, width, height);
		if (this.paddleNumber == 2 || this.paddleNumber == 4)
			g.fillRect(x, y, height, width);
	}

	public void move_up(boolean up)
	{
		int speed = 15;

		if (up)
		{
			if (y - speed -20> 0)
			{
				y -= speed;
			}
			else
			{
				y = 20;
			}
		}
		else
		{
			if (y + height + speed +20 < Pong.pong.height)
			{
				y += speed;
			}
			else
			{
				y = Pong.pong.height - height-20;
			}
		}
	}
	
	public void get_lost()
	{
		this.x = -1000;
		this.y = -1000;
		this.score =0;
	}

	public void move_left(boolean left) {
		int speed = 15;
		if(left)
		{
			if(x-speed-20>0)
			{
				x -= speed;
			}
			else
			{
				x=20;
			}
		}
		else
		{
			if (x+height +speed+20 <Pong.pong.width)
				x += speed;
			else
				x = Pong.pong.width - height-20;
		}
		
	}

}